using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ExcelgensPaybooks.Dtos.Response.Master;
using ExcelgensPaybooks.Dtos.Request;
using ExcelgensPaybooks.Data;
using ExcelgensPaybooks.Models;

namespace ExcelgensPaybooks.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class MasterController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IExcelgensRepository _repo;

        public MasterController(IMapper mapper, IExcelgensRepository repo)
        {
            _mapper = mapper;
            _repo = repo;
        }

        [HttpGet("state")]
        public async Task<IActionResult> GetStates()
        {
            var state = await _repo.GetStates();
            var stateReturn = _mapper.Map<IEnumerable<StateResponse>>(state);
             if(stateReturn==null)
            {
                return BadRequest(new { status = 400, message = "No state found" });
            }     
            return Ok(new { status = 200, state = stateReturn });  
        }
       
        [HttpPost("state")]
        public async Task<IActionResult> InsertState( StateRequest stateRequest)
        {
            
                var stateInsert=_mapper.Map<State>(stateRequest);
                _repo.Add(stateInsert);
                if(await _repo.SaveAll())
                {
                    return Ok(new { status = 200, message = "Sate added successfully" });
                }
                return Ok(new { status = 400, message = "Cannot add State" });
        }

        [HttpPut("state/{id}")]
        public async Task<IActionResult> UpdateState( StateRequest stateRequest,int id)
        {            
            var stateFromRepo=await _repo.GetStateById(id);            
            if(stateFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No state found" });
            }            
            stateFromRepo.Name=stateRequest.Name;
            stateFromRepo.IsActive=stateRequest.IsActive;
            _repo.Update(stateFromRepo);
                if(await _repo.SaveAll())
                {
                    return Ok(new { status = 200, message = "State Updated successfully" });
                }
                return Ok(new { status = 400, message = "Cannot Updated State" });
        }

        [HttpGet("state/{id}")]
        public async Task<IActionResult> EditState(int id)
        {            
            var stateFromRepo=await _repo.GetStateById(id);            
            if(stateFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No state found" });
            }             
             return Ok(new { status = 200, state = stateFromRepo });  

        }
        [HttpDelete("state/{id}")]
        public async Task<IActionResult> DeleteState(int id)
        {            
            var stateFromRepo=await _repo.GetStateById(id);            
            if(stateFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No state found" });
            }  
              _repo.Delete(stateFromRepo);
               if(await _repo.SaveAll())
                {
                    return Ok(new { status = 200, message = "State Deleted successfully" });
                }                
                return BadRequest(new { status = 400, message = "Some Error Occured"});
        }

        [HttpGet("bank")]
        public async Task<IActionResult> GetBanks()
        {
            var banks = await _repo.GetBanks();
            var bankReturn = _mapper.Map<IEnumerable<BankResponse>>(banks);
            return Ok(new { banks = bankReturn });
        }

        [HttpPost("bank")]
        public async Task<IActionResult> InsertBank(BankRequest bankRequest)
        {
            var bankInsert=_mapper.Map<ListofBank>(bankRequest);
            _repo.Add(bankInsert);
            if(await _repo.SaveAll())
            {
                return Ok(new { status = 200, message = "Bank added successfully" });
            }
            return Ok(new { status = 400, message = "Cannot add Bank" });
        }

        [HttpGet("bank/{id}")]
        public async Task<IActionResult> EditBank(int id)
        {            
            var bankFromRepo=await _repo.GetBankById(id);            
            if(bankFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No Bank found" });
            }             
             return Ok(new { status = 200, bank = bankFromRepo });  
        }

        [HttpPut("bank/{id}")]
        public async Task<IActionResult> UpdateBank( BankRequest bankRequest,int id)
        {            
            var bankFromRepo=await _repo.GetBankById(id);            
            if(bankFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No Bank found" });
            }            
            bankFromRepo.Name=bankRequest.Name;
            bankFromRepo.IsActive=bankRequest.IsActive;
            _repo.Update(bankFromRepo);
                if(await _repo.SaveAll())
                {
                    return Ok(new { status = 200, message = "Bank Updated successfully" });
                }
                return Ok(new { status = 400, message = "Cannot Updated Bank" });
        }

        [HttpDelete("bank/{id}")]
        public async Task<IActionResult> DeleteBank(int id)
        {            
            var bankFromRepo=await _repo.GetBankById(id);            
            if(bankFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No Bank found" });
            }  
              _repo.Delete(bankFromRepo);
               if(await _repo.SaveAll())
                {
                    return Ok(new { status = 200, message = "Bank Deleted successfully" });
                }                
                return BadRequest(new { status = 400, message = "Some Error Occured"});
        }

        [HttpGet("department")]
        public async Task<IActionResult> GetDepartments()
        {
            var departments = await _repo.GetDepartments();
            var departmentList = _mapper.Map<IEnumerable<DepartmentResponse>>(departments);
            return Ok(new { departments = departmentList });
        }

        [HttpPost("department")]
        public async Task<IActionResult> InsertDepartment(DepartmentRequest departmentRequest)
        {
            var departmentInsert=_mapper.Map<Department>(departmentRequest);
            _repo.Add(departmentInsert);
            if(await _repo.SaveAll())
            {
                return Ok(new { status = 200, message = "Department added successfully" });
            }
            return Ok(new { status = 400, message = "Cannot add Department" });
        }

        [HttpGet("department/{id}")]
        public async Task<IActionResult> EditDepartment(int id)
        {            
            var departmentFromRepo=await _repo.GetDepartmentById(id);            
            if(departmentFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No Department found" });
            }             
             return Ok(new { status = 200, bank = departmentFromRepo });  
        }

        [HttpPut("department/{id}")]
        public async Task<IActionResult> UpdateDepartment(DepartmentRequest departmentRequest,int id)
        {            
            var departmentFromRepo=await _repo.GetDepartmentById(id);            
            if(departmentFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No Bank found" });
            }            
            departmentFromRepo.Name=departmentRequest.Name;
            departmentFromRepo.IsActive=departmentRequest.IsActive;
            _repo.Update(departmentFromRepo);
                if(await _repo.SaveAll())
                {
                    return Ok(new { status = 200, message = "Department Updated successfully" });
                }
                return Ok(new { status = 400, message = "Cannot Updated Department" });
        }

        [HttpDelete("department/{id}")]
        public async Task<IActionResult> DeleteDepartment(int id)
        {            
            var departmentFromRepo=await _repo.GetDepartmentById(id);            
            if(departmentFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No Department found" });
            }  
              _repo.Delete(departmentFromRepo);
               if(await _repo.SaveAll())
                {
                    return Ok(new { status = 200, message = "Department Deleted successfully" });
                }                
                return BadRequest(new { status = 400, message = "Some Error Occured"});
        }



        [HttpGet("bloodGroup")]
        public async Task<IActionResult> GetBloodGroups()
        {
            var Blood = await _repo.GetBloodGroups();
            var BloodGroupReturn = _mapper.Map<IEnumerable<BloodGroupResponse>>(Blood);

            if(BloodGroupReturn==null)
            {
                return BadRequest(new { status = 400, message = "No Blood Group found" });
            }     
            return Ok(new { status = 200, bloodGroup = BloodGroupReturn }); 
        }




        [HttpPost("bloodGroup")]
        public async Task<IActionResult> InsertBloodGroup(BloodGroupRequest bloodGroupRequest)
        {
            
                var bloodGroupInsert=_mapper.Map<BloodGroup>(bloodGroupRequest);
                _repo.Add(bloodGroupInsert);
                if(await _repo.SaveAll())
                {
                    return Ok(new { status = 200, message = "Blood Group added successfully" });
                }
                return Ok(new { status = 400, message = "Cannot add Blood Group" });
        }

        [HttpGet("bloodGroup/{id}")]
        public async Task<IActionResult> EditBloodGroup(int id)
        {            
            var bloodGroupFromRepo=await _repo.GetBloodGroupById(id);            
            if(bloodGroupFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No Blood Group found" });
            }             
             return Ok(new { status = 200, bloodGroup = bloodGroupFromRepo });  

        }


         [HttpDelete("bloodGroup/{id}")]
        public async Task<IActionResult> DeleteBloodGroup(int id)
        {            
            var bloodGroupFromRepo=await _repo.GetBloodGroupById(id);            
            if(bloodGroupFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No Blood Group found" });
            }  
              _repo.Delete(bloodGroupFromRepo);
               if(await _repo.SaveAll())
                {
                    return Ok(new { status = 200, message = "Blood Group Deleted successfully" });
                }                
                return BadRequest(new { status = 400, message = "Some Error Occured"});
        }

        [HttpPut("bloodGroup/{id}")]
        public async Task<IActionResult> UpdateBloodGroup( BloodGroupRequest bloodGroupRequest,int id)
        {            
            var bloodGroupFromRepo=await _repo.GetBloodGroupById(id);            
            if(bloodGroupFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No Blood Group found" });
            }            
            bloodGroupFromRepo.Type=bloodGroupRequest.Type;
            bloodGroupFromRepo.IsActive=bloodGroupRequest.IsActive;
            _repo.Update(bloodGroupFromRepo);
                if(await _repo.SaveAll())
                {
                    return Ok(new { status = 200, message = "Blood Group Updated successfully" });
                }
                return Ok(new { status = 400, message = "Cannot Updated Blood Group" });
        }

        [HttpGet("designation")]
        public async Task<IActionResult> GetDesignations()
        {
            var designations = await _repo.GetDesignations();
            var designationList = _mapper.Map<IEnumerable<DesignationResponse>>(designations);
            return Ok(new { designation = designationList });
        }

        [HttpPost("designation")]
        public async Task<IActionResult> InsertDesignation(DesignationRequest designationRequest)
        {
            var designationInsert=_mapper.Map<Designation>(designationRequest);
            _repo.Add(designationInsert);
            if(await _repo.SaveAll())
            {
                return Ok(new { status = 200, message = "Designation added successfully" });
            }
            return Ok(new { status = 400, message = "Cannot add Designation" });
        }

        [HttpGet("designation/{id}")]
        public async Task<IActionResult> EditDesignation(int id)
        {            
            var designationFromRepo=await _repo.GetDesignationById(id);            
            if(designationFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No Designation found" });
            }             
             return Ok(new { status = 200, bank = designationFromRepo });  
        }

        [HttpPut("designation/{id}")]
        public async Task<IActionResult> UpdateDesignation(DesignationRequest designationRequest,int id)
        {            
            var designationFromRepo=await _repo.GetDesignationById(id);            
            if(designationFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No Designation found" });
            }            
            designationFromRepo.Name=designationRequest.Name;
            designationFromRepo.IsActive=designationRequest.IsActive;
            _repo.Update(designationFromRepo);
                if(await _repo.SaveAll())
                {
                    return Ok(new { status = 200, message = "Designation Updated successfully" });
                }
            return Ok(new { status = 400, message = "Cannot Updated Designation" });
        }

        [HttpDelete("designation/{id}")]
        public async Task<IActionResult> DeleteDesignation(int id)
        {            
            var designationFromRepo=await _repo.GetDesignationById(id);            
            if(designationFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No Designation found" });
            }  
              _repo.Delete(designationFromRepo);
               if(await _repo.SaveAll())
                {
                    return Ok(new { status = 200, message = "Designation Deleted successfully" });
                }                
            return BadRequest(new { status = 400, message = "Some Error Occured"});
        }


        [HttpGet("holidays")]
        public async Task<IActionResult> GetHolidays()
        {
            var Holidays = await _repo.GetHolidays();
            var HolidaysReturn = _mapper.Map<IEnumerable<HolidaysResponse>>(Holidays);

            if(HolidaysReturn== null)
            {
                return BadRequest(new { status = 400, message = "No Holidays found" });
            }     
            return Ok(new { status = 200, Holidays = HolidaysReturn }); 
        }
        
        [HttpPost("holidays")]
        public async Task<IActionResult> InsertHolidays(HolidaysRequest holidaysRequest)
        {
            
                var holidaysInsert=_mapper.Map<Holidays>(holidaysRequest);
                _repo.Add(holidaysInsert);
                if(await _repo.SaveAll())
                {
                    return Ok(new { status = 200, message = "Holidays added successfully" });
                }
                return Ok(new { status = 400, message = "Cannot add Holidays" });
        }



        [HttpGet("holidays/{id}")]
        public async Task<IActionResult> EditHolidays(int id)
        {            
            var holidaysFromRepo=await _repo.GetHolidaysById(id);            
            if(holidaysFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No Holidays found" });
            }             
             return Ok(new { status = 200, holidays = holidaysFromRepo });  

        }


        [HttpDelete("holidays/{id}")]
        public async Task<IActionResult> DeleteHolidays(int id)
        {            
            var holidaysFromRepo=await _repo.GetHolidaysById(id);            
            if(holidaysFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No Holidays found" });
            }  
              _repo.Delete(holidaysFromRepo);
               if(await _repo.SaveAll())
                {
                    return Ok(new { status = 200, message = "Holidays Deleted successfully" });
                }                
                return BadRequest(new { status = 400, message = "Some Error Occured"});
        }



        [HttpPut("holidays/{id}")]
        public async Task<IActionResult> UpdateHolidays(HolidaysRequest holidaysRequest,int id)
        {            
            var holidaysFromRepo=await _repo.GetHolidaysById(id);            
            if(holidaysFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No Holidays found" });
            }            
			holidaysFromRepo.Date=holidaysRequest.Date;
			holidaysFromRepo.Name=holidaysRequest.Name;
			holidaysFromRepo.IsActive=holidaysRequest.IsActive;
            _repo.Update(holidaysFromRepo);
                if(await _repo.SaveAll())
                {
                    return Ok(new { status = 200, message = "holidays Updated successfully" });
                }
                return Ok(new { status = 400, message = "Cannot Updated holidays" });
        }

        [HttpGet("leaveType")]
        public async Task<IActionResult> GetLeaveType()
        {
            var leaveType = await _repo.GetLeaveType();
            var leaveTypeList = _mapper.Map<IEnumerable<LeaveTypeResponse>>(leaveType);
            return Ok(new { leaveType = leaveTypeList });
        }

        [HttpPost("leaveType")]
        public async Task<IActionResult> InsertLeaveType(LeaveTypeRequest leaveTypeRequest)
        {
            var leaveTypeInsert=_mapper.Map<LeaveType>(leaveTypeRequest);
            _repo.Add(leaveTypeInsert);
            if(await _repo.SaveAll())
            {
                return Ok(new { status = 200, message = "LeaveType added successfully" });
            }
            return Ok(new { status = 400, message = "Cannot add LeaveType" });
        }

        [HttpGet("leaveType/{id}")]
        public async Task<IActionResult> EditLeaveType(int id)
        {            
            var leaveTypeFromRepo=await _repo.GetLeaveTypeById(id);            
            if(leaveTypeFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No Designation found" });
            }             
             return Ok(new { status = 200, leaveType = leaveTypeFromRepo });  
        }

        [HttpPut("leaveType/{id}")]
        public async Task<IActionResult> UpdateLeaveType(LeaveTypeRequest leaveTypeRequest,int id)
        {            
            var leaveTypeFromRepo=await _repo.GetLeaveTypeById(id);            
            if(leaveTypeFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No LeaveType found" });
            }            
            leaveTypeFromRepo.Type=leaveTypeRequest.Type;
            leaveTypeFromRepo.Balance=leaveTypeRequest.Balance;
            leaveTypeFromRepo.IsActive=leaveTypeRequest.IsActive;
            _repo.Update(leaveTypeFromRepo);
                if(await _repo.SaveAll())
                {
                    return Ok(new { status = 200, message = "LeaveType Updated successfully" });
                }
            return Ok(new { status = 400, message = "Cannot Updated LeaveType" });
        }

        [HttpDelete("leaveType/{id}")]
        public async Task<IActionResult> DeleteLeaveType(int id)
        {            
            var leaveTypeFromRepo=await _repo.GetLeaveTypeById(id);            
            if(leaveTypeFromRepo==null)
            {
                return BadRequest(new { status = 400, message = "No LeaveType found" });
            }  
              _repo.Delete(leaveTypeFromRepo);
               if(await _repo.SaveAll())
                {
                    return Ok(new { status = 200, message = "LeaveType Deleted successfully" });
                }                
            return BadRequest(new { status = 400, message = "Some Error Occured"});
        }

    }
}