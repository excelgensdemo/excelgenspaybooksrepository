using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using ExcelgensPaybooks.Models;
using Microsoft.Extensions.Configuration;
using ExcelgensPaybooks.Data;
using ExcelgensPaybooks.Dtos.Response;
using ExcelgensPaybooks.Helpers;
using ExcelgensPaybooks.Dtos.Request;



namespace ExcelgensPaybooks.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;
        private readonly IExcelgensRepository _repo;
        private readonly UserManager<Users> _userManager;
        private readonly SignInManager<Users> _signInManager;
        private readonly DataContext _context;
        // private readonly IEmailService _emailService;
        public AuthController(IConfiguration config,
            IMapper mapper, IExcelgensRepository repo,
            UserManager<Users> userManager, DataContext context,
            SignInManager<Users> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
            _repo = repo;
            _config = config;
            _context = context;
            // _emailService = emailService;
        }

        [HttpPost("Employee")]
        public async Task<IActionResult> RegisterEmployee(EmployeeRequest userForRegisterEmployeeDto)
        {
            userForRegisterEmployeeDto.UserName = userForRegisterEmployeeDto.Email;

            var userExists = await _userManager.FindByEmailAsync(userForRegisterEmployeeDto.Email);
            if (userExists != null)
            {
                return Ok(new { Status = 400, message = "Cannot register user " + userForRegisterEmployeeDto.Email });
            }

            var userToCreate = _mapper.Map<Users>(userForRegisterEmployeeDto);

            var result = await _userManager.CreateAsync(userToCreate, userForRegisterEmployeeDto.Password);

            var userToReturn = _mapper.Map<EmployeeResponse>(userToCreate);

            if (result.Succeeded)
            {
                var userRole = _userManager.AddToRoleAsync(userToCreate, "Employee").Result;
                var currentUser = await _userManager.FindByEmailAsync(userForRegisterEmployeeDto.Email);

                string confirmationToken = await _userManager.GenerateEmailConfirmationTokenAsync(userToCreate);
                userToReturn.EmailToken = confirmationToken;
                // To Insert Data in Employee Offical Detail table 
                var employeeOfficeDetails=new EmployeeOfficeDetails();
                employeeOfficeDetails.EmployeeId=userToCreate.Id;
                employeeOfficeDetails.BankId=userForRegisterEmployeeDto.EmployeeOfficeDetails.BankId;
                employeeOfficeDetails.JoiningDate=userForRegisterEmployeeDto.EmployeeOfficeDetails.JoiningDate;
                employeeOfficeDetails.ConfirmationDate=userForRegisterEmployeeDto.EmployeeOfficeDetails.ConfirmationDate;
                employeeOfficeDetails.JobType=userForRegisterEmployeeDto.EmployeeOfficeDetails.JobType;
                employeeOfficeDetails.LocationID=userForRegisterEmployeeDto.EmployeeOfficeDetails.LocationID;
                employeeOfficeDetails.PaymentMode=userForRegisterEmployeeDto.EmployeeOfficeDetails.PaymentMode;
                employeeOfficeDetails.PersonalBankAccountNumber=userForRegisterEmployeeDto.EmployeeOfficeDetails.PersonalBankAccountNumber;
                employeeOfficeDetails.IFSCCode=userForRegisterEmployeeDto.EmployeeOfficeDetails.IFSCCode;
                employeeOfficeDetails.Grade=userForRegisterEmployeeDto.EmployeeOfficeDetails.Grade;
                employeeOfficeDetails.FirstReportingManager=userForRegisterEmployeeDto.EmployeeOfficeDetails.FirstReportingManager;
                employeeOfficeDetails.SecondReportingManager=userForRegisterEmployeeDto.EmployeeOfficeDetails.SecondReportingManager;
                employeeOfficeDetails.SalaryPaymentMode=userForRegisterEmployeeDto.EmployeeOfficeDetails.SalaryPaymentMode;
employeeOfficeDetails.EmployeeSalaryBankAccount=userForRegisterEmployeeDto.EmployeeOfficeDetails.EmployeeSalaryBankAccount;
employeeOfficeDetails.REmployeeSalaryAccountNumber=userForRegisterEmployeeDto.EmployeeOfficeDetails.REmployeeSalaryAccountNumber;
employeeOfficeDetails.SalaryPaymentMode=userForRegisterEmployeeDto.EmployeeOfficeDetails.PaySalaryFrom;
employeeOfficeDetails.IncrementDate=userForRegisterEmployeeDto.EmployeeOfficeDetails.IncrementDate;
employeeOfficeDetails.ResignationDate=userForRegisterEmployeeDto.EmployeeOfficeDetails.ResignationDate;
employeeOfficeDetails.SalaryPaymentMode=userForRegisterEmployeeDto.EmployeeOfficeDetails.LastWorkingDate;
                


                _repo.Add(employeeOfficeDetails);
               
               // To Insert Data in Nominee Detail table 
                var nomineeDetail=new NominieDetail();
                nomineeDetail.EmployeeId=userToCreate.Id;
                nomineeDetail.Name=userForRegisterEmployeeDto.NominieDetail.Name;
                nomineeDetail.Relation=userForRegisterEmployeeDto.NominieDetail.Relation;
                _repo.Add(nomineeDetail);

               // To Insert Data in PFESI Detail table 
                var employeePFESIDetails=new EmployeePFandESI();
                employeePFESIDetails.EmployeeId=userToCreate.Id;
                employeePFESIDetails.OnehasPF=userForRegisterEmployeeDto.EmployeePFESIRequest.OnehasPF;
                employeePFESIDetails.PFUAN=userForRegisterEmployeeDto.EmployeePFESIRequest.PFUAN;
                employeePFESIDetails.PFNumber=userForRegisterEmployeeDto.EmployeePFESIRequest.PFNumber;
                employeePFESIDetails.OnehasESI=userForRegisterEmployeeDto.EmployeePFESIRequest.OnehasESI;
                employeePFESIDetails.ESINumber=userForRegisterEmployeeDto.EmployeePFESIRequest.ESINumber;
                employeePFESIDetails.PFEnrollementDate=userForRegisterEmployeeDto.EmployeePFESIRequest.PFEnrollementDate;
                _repo.Add(employeePFESIDetails);

                // To Insert Data in Salary Detail table 
                var salaryDetails=new Salarydetails();
                c.EmployeeId=userToCreate.Id;
                salaryDetails.Basic=userForRegisterEmployeeDto.SalarydetailsRequest.Basic;
                salaryDetails.HRA=userToCreate.HRA;
                salaryDetails.MedicalAllowance=userToCreate.MedicalAllowance;
                salaryDetails.PF=userToCreate.PF;
                salaryDetails.ESi=userToCreate.ESi;
                _repo.Add(salaryDetails);
                
                
                if (await _repo.SaveAll())
                {
                   // confirmationToken = await _userManager.GenerateEmailConfirmationTokenAsync(currentUser);
                    //ConfirmEmail(currentUser.Id, confirmationToken);
                    try
                    {
                        // SendEmail(currentUser.FirstName + " " + currentUser.LastName
                        //     ,currentUser.Email,
                        //     "ExcelgensPaybooks - OTP",
                        //         "Your email OTP for email confirmation is " + confirmationToken);
                    }
                    catch
                    {

                    }
                    return Ok(new { status = 200, message = "User registered successfully", user = userToReturn });
                }
            }
            return BadRequest(new { Status = 400, message = "Cannot register user " + userForRegisterEmployeeDto.Email });
        }

        public void SendEmail(string name, string emailAddress, string subject, string content)
        {
            var email = new EmailMessage();
            email.FromAddresses = new List<EmailAddress>() { new EmailAddress() { Name = "Excelgens", Address = "excelgensusa@gmail.com" } };
            email.ToAddresses = new List<EmailAddress>() { new EmailAddress() { Name = name, Address = emailAddress } };
            email.Subject = subject;
            email.Content = content;
            // _emailService.SendAsync(email);
        }



     


    }
}