namespace ExcelgensPaybooks.Dtos.Response.Master
{
    public class BankResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public int IsActive { get; set; }
    }
}