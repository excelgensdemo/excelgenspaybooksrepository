namespace ExcelgensPaybooks.Dtos.Response.Master
{
    public class DesignationResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int IsActive { get; set; }
    }
}