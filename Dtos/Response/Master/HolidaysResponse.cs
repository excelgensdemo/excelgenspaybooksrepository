using System;

namespace ExcelgensPaybooks.Dtos.Response.Master
{
    public class HolidaysResponse
    {
    public int Id { get; set; }
    public DateTime? Date { get; set; }
    public string Name { get; set; }
    public bool IsActive { get; set; }
    }
}