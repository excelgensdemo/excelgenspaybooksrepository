namespace ExcelgensPaybooks.Dtos.Response.Master
{
    public class BloodGroupResponse
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public bool IsActive { get; set; }
    }
}