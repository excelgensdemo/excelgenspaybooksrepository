namespace ExcelgensPaybooks.Dtos.Response.Master
{
    public class StateResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Message { get; set; }
        public int IsActive { get; set; } 
    }
}