using System;
using System.Collections.Generic;
using ExcelgensPaybooks.Models;

namespace ExcelgensPaybooks.Dtos.Response
{
    public class EmployeeResponse
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public DateTime Created { get; set; }
        public string City { get; set; }
        public string StateId { get; set; }
        public string EmailToken { get; set; }
    }
}