using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ExcelgensPaybooks.Models;

namespace ExcelgensPaybooks.Dtos.Request
{
    public  class EmployeePFESIRequest
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public Boolean OnehasPF  { get; set; }

        public Boolean OnehasESI { get; set; }

        public string PFUAN   { get; set; }

        public string PFNumber  { get; set; }

        public string ESINumber  { get; set; }

        public DateTime PFEnrollementDate   { get; set; }

    }
}