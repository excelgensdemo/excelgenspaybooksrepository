using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ExcelgensPaybooks.Models;

namespace ExcelgensPaybooks.Dtos.Request
{
    public class EmployeeRequest
    {
        [Required]
        public string Email { get; set; }

        public string UserName { get; set; }

        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }

        [Required]
        public string Phone { get; set; }

        [Required]
        //[StringLength(8, MinimumLength = 4, ErrorMessage = "You must specify a password between 4 and 8 characters")]
        public string Password { get; set; }

        public DateTime Created { get; set; }

        //public CustomerVehicleForRegisterDto CustomerVehicles { get; set; }
        public EmployeeRequest()
        {
            Created = DateTime.Now;
        }
        public virtual NominieDetailRequest NominieDetail { get; set; }
        public virtual EmployeeOfficeDetailsRequest EmployeeOfficeDetails { get; set; }
        public virtual EmployeePFESIRequest EmployeePFESIRequest { get; set; }
        
        public virtual SalarydetailsRequest SalarydetailsRequest { get; set; }

    }
}