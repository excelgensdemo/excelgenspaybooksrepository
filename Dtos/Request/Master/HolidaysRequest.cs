using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ExcelgensPaybooks.Models;

namespace ExcelgensPaybooks.Dtos.Request
{
    public class HolidaysRequest
    {        
        public int Id { get; set; }
        public DateTime? Date { get; set; }  
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
