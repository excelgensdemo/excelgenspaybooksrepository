using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ExcelgensPaybooks.Models;

namespace ExcelgensPaybooks.Dtos.Request
{
    public class LeaveTypeRequest
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public double Balance { get; set; }
        public bool IsActive { get; set; }       
        public int LeaveCount { get; set; } 
    }
}