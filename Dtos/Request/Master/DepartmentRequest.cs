using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ExcelgensPaybooks.Models;
using ExcelgensPaybooks.Helpers;

namespace ExcelgensPaybooks.Dtos.Request
{
    public class DepartmentRequest
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public bool IsActive { get; set; }     
    }
}