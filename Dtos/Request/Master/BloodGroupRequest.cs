using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ExcelgensPaybooks.Models;

namespace ExcelgensPaybooks.Dtos.Request
{
    public class BloodGroupRequest
    {        
        public int Id { get; set; }
        [Required]
        public string Type { get; set; }
        public bool IsActive { get; set; }
    }
}
