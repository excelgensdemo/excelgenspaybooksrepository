using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ExcelgensPaybooks.Models;

namespace ExcelgensPaybooks.Dtos.Request
{
 public class EmployeeOfficeDetailsRequest
 {
        public int EmployeeId { get; set; }

        public DateTime JoiningDate  { get; set; }

        public DateTime ConfirmationDate   { get; set; }

        public string JobType  { get; set; }

        public int LocationID  { get; set; }

        public string PaymentMode  { get; set; }

        public int BankId  { get; set; }

        public string PersonalBankAccountNumber   { get; set; }

        public string IFSCCode  { get; set; }

        public string Grade   { get; set; }

        public string FirstReportingManager  { get; set; }

        public string SecondReportingManager  { get; set; }

        public string SalaryPaymentMode   { get; set; }

        public string EmployeeSalaryBankAccount   { get; set; }

        public string REmployeeSalaryAccountNumber   { get; set; }

        public string PaySalaryFrom   { get; set; }

        public DateTime IncrementDate   { get; set; }

        public DateTime ResignationDate   { get; set; }

        public DateTime LastWorkingDate   { get; set; }
 }
}