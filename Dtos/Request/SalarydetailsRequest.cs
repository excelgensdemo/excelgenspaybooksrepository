using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace ExcelgensPaybooks.Dtos.Request
{
    public class SalarydetailsRequest
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }
        public string Basic { get; set; }

        public string HRA { get; set; }

        public string MedicalAllowance { get; set; }

        public string PF { get; set; }

        public string ESi { get; set; }


    }

}