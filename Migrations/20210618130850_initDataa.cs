﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ExcelgensPaybooks.Migrations
{
    public partial class initDataa : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EmployeePFandESI",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmployeeId = table.Column<int>(type: "int", nullable: false),
                    OnehasPF = table.Column<bool>(type: "bit", nullable: false),
                    OnehasESI = table.Column<bool>(type: "bit", nullable: false),
                    PFUAN = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PFNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ESINumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PFEnrollementDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeePFandESI", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Salarydetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmployeeId = table.Column<int>(type: "int", nullable: false),
                    Basic = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    HRA = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MedicalAllowance = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PF = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ESi = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Salarydetails", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmployeePFandESI");

            migrationBuilder.DropTable(
                name: "Salarydetails");
        }
    }
}
