using System;
using System.Collections.Generic;
namespace ExcelgensPaybooks.Models
{
    public partial class LeaveType
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public double Balance { get; set; }
        public bool IsActive { get; set; }       
        public int LeaveCount { get; set; }

    }
}