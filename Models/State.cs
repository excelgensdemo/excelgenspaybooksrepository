using System;
using System.Collections.Generic;
namespace ExcelgensPaybooks.Models
{
    public partial class State
    {
        public State()
        {
            Users = new HashSet<Users>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<Users> Users { get; set; }
    }
}