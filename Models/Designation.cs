using System;
using System.Collections.Generic;
namespace ExcelgensPaybooks.Models
{
    public partial class Designation
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; } 
    }
}