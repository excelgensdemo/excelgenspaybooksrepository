using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace ExcelgensPaybooks.Models
{
    public partial class Users : IdentityUser<int>
    {
        public Users()
        {
             
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public DateTime DOB { get; set; }
        public int Gender { get; set; }
        public string  MaritalStatus { get; set; }
        public string ResidentialAddress { get; set; }
        public string CorrespondenceAddress { get; set; }
        public string City { get; set; }
        public int? StateId { get; set; }
        public int DepartmentId { get; set; }
        public int DesignationId { get; set; }
        public int EmployeeCode { get; set; }
        public string PinCode { get; set; }
        public bool IsMarried { get; set; }
        public string SpouseName { get; set; }
        public bool TermsandCondition { get; set; }
        public bool IsUserDeleted { get; set; }
        public DateTime? Created { get; set; }
        public int? Status { get; set; }

        public virtual State State { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
