using System;
using System.Collections.Generic;
namespace ExcelgensPaybooks.Models
{
    public partial class NominieDetail
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public string Name { get; set; }

        public string Relation { get; set; }

        public string NominieDocument { get; set; }


    }
}