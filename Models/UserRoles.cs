using Microsoft.AspNetCore.Identity;

namespace ExcelgensPaybooks.Models
{
    public class UserRole : IdentityUserRole<int>
    {
        public virtual Users User { get; set; }
        public virtual Role Role { get; set; }
    }
}