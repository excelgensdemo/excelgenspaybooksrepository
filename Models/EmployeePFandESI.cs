using System;
using System.Collections.Generic;
namespace ExcelgensPaybooks.Models
{
    public partial class EmployeePFandESI
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public Boolean OnehasPF  { get; set; }

        public Boolean OnehasESI { get; set; }

        public string PFUAN   { get; set; }

        public string PFNumber  { get; set; }

        public string ESINumber  { get; set; }

        public DateTime PFEnrollementDate   { get; set; }

    }
}