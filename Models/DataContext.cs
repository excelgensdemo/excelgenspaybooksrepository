using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ExcelgensPaybooks.Models;

namespace ExcelgensPaybooks.Models
{
    public class DataContext : IdentityDbContext<Users, Role, int, IdentityUserClaim<int>, UserRole, IdentityUserLogin<int>, IdentityRoleClaim<int>, IdentityUserToken<int>>
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<Designation> Designation { get; set; }       
        public virtual DbSet<LeaveType> LeaveType { get; set; }
        public virtual DbSet<Holidays> Holidays { get; set; }
        public virtual DbSet<ListofBank> ListofBank { get; set; }
        public virtual DbSet<BloodGroup> BloodGroup { get; set; }
        public virtual DbSet<State> State { get; set; } 
        public virtual DbSet<NominieDetail> NominieDetail {get;set;}      
        public virtual DbSet<EmployeeOfficeDetails> EmployeeOfficeDetails {get; set;}

        public virtual DbSet<EmployeePFandESI> EmployeePFandESI {get; set;}

        public virtual DbSet<Salarydetails> Salarydetails {get; set;}

        //fluent api 
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<UserRole>(userRole =>
            {
                userRole.HasKey(ur => new { ur.UserId, ur.RoleId });
                

                userRole.HasOne(ur => ur.Role)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();

                userRole.HasOne(ur => ur.User)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });

            builder.Entity<Users>(entity =>
            {
                entity.Property(e => e.City).HasMaxLength(150);

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.FirstName).HasMaxLength(150);

                entity.Property(e => e.LastName).HasMaxLength(150);

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.Property(e => e.PinCode).HasMaxLength(50); 
                
                entity.HasOne(d => d.State)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_Users_State");
            });
        }
    }

}