using System;
using System.Collections.Generic;
namespace ExcelgensPaybooks.Models
{
public partial class Holidays
    {
        public int Id { get; set; }
        public DateTime? Date { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}