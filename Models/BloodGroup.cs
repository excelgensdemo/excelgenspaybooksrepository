using System;
using System.Collections.Generic;
namespace ExcelgensPaybooks.Models
{
public partial class BloodGroup
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public bool IsActive { get; set; }
    }
}