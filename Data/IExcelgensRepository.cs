using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExcelgensPaybooks.Models;
using ExcelgensPaybooks.Data;

namespace ExcelgensPaybooks.Data
{
    public interface IExcelgensRepository
    {
        void Add<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        void Update<T>(T entity) where T : class;
        Task<bool> SaveAll();
        Task<IEnumerable<State>> GetStates();
        Task<State> GetStateById(int Id);
        Task<IEnumerable<Department>> GetDepartments();
        Task<Department> GetDepartmentById(int Id);
        Task<IEnumerable<Designation>> GetDesignations();
        Task<Designation> GetDesignationById(int Id);
        Task<IEnumerable<ListofBank>> GetBanks();
        Task<ListofBank> GetBankById(int Id);  
        Task<IEnumerable<BloodGroup>> GetBloodGroups();
        Task<BloodGroup> GetBloodGroupById(int Id);
        Task<IEnumerable<Holidays>> GetHolidays();
        Task<Holidays> GetHolidaysById(int Id);
        Task<IEnumerable<LeaveType>> GetLeaveType();
        Task<LeaveType> GetLeaveTypeById(int Id);

    }
}