using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExcelgensPaybooks.Models;
using ExcelgensPaybooks.Helpers;
using Microsoft.EntityFrameworkCore;
using ExcelgensPaybooks.Data;

namespace ExcelgensPaybooks.Data
{
    public class ExcelgensRepository:IExcelgensRepository
    {
        private readonly DataContext _context;
        public ExcelgensRepository(DataContext context)
        {
            _context = context;
        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public void Update<T>(T entity) where T : class
        {
            _context.Update(entity);
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<IEnumerable<State>> GetStates()
        {
            return await _context.State.OrderBy(o => o.Name).ToListAsync();
        }
        public async Task<IEnumerable<ListofBank>> GetBanks()
        {
            return await _context.ListofBank.OrderBy(o => o.Name).ToListAsync();
        }
        public async Task<IEnumerable<Department>> GetDepartments()
        {
            return await _context.Department.OrderBy(o => o.Name).ToListAsync();
        }
        public async Task<IEnumerable<Designation>> GetDesignations()
        {
            return await _context.Designation.OrderBy(o => o.Name).ToListAsync();
        }
        public async Task<Designation> GetDesignationById(int Id)
        {
            return await _context.Designation.FirstOrDefaultAsync(o => o.Id == Id);
        } 

        public async Task<State> GetStateById(int Id)
        {
            return await _context.State.FirstOrDefaultAsync(o => o.Id == Id);
        } 

        public async Task<ListofBank> GetBankById(int Id)
        {
            return await _context.ListofBank.FirstOrDefaultAsync(o => o.Id == Id);
        } 
        public async Task<Department> GetDepartmentById(int Id)
        {
            return await _context.Department.FirstOrDefaultAsync(o => o.Id == Id);
        } 

        public async Task<IEnumerable<BloodGroup>> GetBloodGroups()
        {
            return await _context.BloodGroup.OrderBy(o => o.Type).ToListAsync();
        }

        public async Task<BloodGroup> GetBloodGroupById(int Id)
        {
          return await _context.BloodGroup.FirstOrDefaultAsync(o => o.Id == Id);
        } 

        public async Task<IEnumerable<Holidays>> GetHolidays()
        {
        return await _context.Holidays.OrderBy(o => o.Name).ToListAsync();
        }
        public async Task<Holidays> GetHolidaysById(int Id)
        {
        return await _context.Holidays.FirstOrDefaultAsync(o => o.Id == Id);
        } 
        public async Task<IEnumerable<LeaveType>> GetLeaveType()
        {
            return await _context.LeaveType.OrderBy(o => o.Type).ToListAsync();
        }
        public async Task<LeaveType> GetLeaveTypeById(int Id)
        {
            return await _context.LeaveType.FirstOrDefaultAsync(o => o.Id == Id);
        } 
    }
}