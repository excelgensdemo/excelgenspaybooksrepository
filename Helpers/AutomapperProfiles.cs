using System;
using System.Linq;
using AutoMapper;
using ExcelgensPaybooks.Models;
using ExcelgensPaybooks.Dtos.Response;
using ExcelgensPaybooks.Dtos.Request;
using ExcelgensPaybooks.Dtos.Response.Master;


namespace ExcelgensPaybooks.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Users, EmployeeResponse>();
            CreateMap<Users,EmployeeRequest>().ReverseMap();
            CreateMap<State, StateResponse>();
            CreateMap<State, StateRequest>().ReverseMap();
            CreateMap<ListofBank, BankResponse>();
            CreateMap<Department,DepartmentResponse>();
            CreateMap<Department,DepartmentRequest>().ReverseMap();
            CreateMap<Designation,DesignationResponse>();
            CreateMap<Designation,DesignationRequest>().ReverseMap();
            CreateMap<BloodGroup, BloodGroupResponse>();
            CreateMap<BloodGroup, BloodGroupRequest>().ReverseMap();
            CreateMap<Holidays, HolidaysResponse>();
            CreateMap<Holidays, HolidaysRequest>().ReverseMap();
            CreateMap<LeaveType, LeaveTypeResponse>();
            CreateMap<LeaveType, LeaveTypeRequest>().ReverseMap();
            CreateMap<Users, NominieDetailRequest>();
        }
    }
}